###BUILT IN EXCEPTION
#Index Error
try:  
    a = ['a', 'b', 'c']  
    print (a[4])  
except LookupError:  
    print ("Index Error Exception Raised, list index out of range")
else:  
    print ("Success, no error!")
    
#Overflow Error    
try:  
    import math
    print(math.exp(1000))
except OverflowError:  
        print ("OverFlow Exception Raised.")
else:  
    print ("Success, no error!")
    
#Key Error   
try:  
    a = {1:'a', 2:'b', 3:'c'}  
    print (a[4])  
except LookupError:  
    print ("Key Error Exception Raised.")
else:  
    print ("Success, no error!")

#Name Error
try:
    print (ans)
except NameError:  
    print ("NameError: name 'ans' is not defined")
else:  
    print ("Success, no error!")
    
#Attribute Error
class Attributes:
    a = 2
try:
    object = Attributes()
    print (object.attribute)
except AttributeError:
    print ("Attribute Exception Raised.")
