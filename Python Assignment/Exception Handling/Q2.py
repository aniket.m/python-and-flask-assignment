class personInfo:
    
    def __init__(self, name, age, email , phoneNumber):  
        
        if len(name) > 7:
            raise InvalidNameError(name)
        else:
            self.name = name
            
        if age < 0:
            raise ageError(age)
        else:
            self.age = age
            
        if ('.' in email):
            self.email = email
        else:
            raise emailError(email)
        
        if len(str(phoneNumber) is not 10):
            raise phoneError(phoneNumber)
        else:
            self.phoneNumber = phoneNumber
        
class InvalidNameError(Exception):
    
    def __init__(self, name, message="Name Error"):
        self.name = name
        self.message = message
        super().__init__(self.message)
        
class ageError(Exception):
    
    def __init__(self, age, message="Invalid age"):
        self.age = age
        self.message = message
        super().__init__(self.message)

class emailError(Exception):
    
    def __init__(self, email, message="Invalid email"):
        self.email = email
        self.message = message
        super().__init__(self.message)
        
class phoneError(Exception):
    
    def __init__(self, phoneNumber, message="Invalid Phone Number"):
        self.phoneNumber = phoneNumber
        self.message = message
        super().__init__(self.message)
        

name = input("Enter name (to raise exception enter more than 7 letter) : ")
age = int(input("Enter Age (to raise exception enter negative number) : "))
email = str(input("Enter email (to raise exception skip . in mail id) : "))
phoneNumber = int(input("Enter Phone No. (to raise exception enter less or more than 10 digit) : "))
person1 = personInfo(name , age , email , phoneNumber)
                  