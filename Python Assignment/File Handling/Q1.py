str = "This text will writen in file.\n"

file = open("File.txt","a") 
file.write(str)
file.close()

file = open("File.txt","r+")
lines = file.readlines()
file.close()

for i in range(len(lines)):
    print(lines[i])

file = open("File.txt","a")
file.write("This text will writen in file..\n")
file.close()

file = open("File.txt","r+")
lines = file.readlines()
file.close()

for i in range(len(lines)):
    print(lines[i])