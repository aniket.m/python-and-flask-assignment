import csv
import random

all = []

with open('test.csv','r') as csvinput:
    
    reader = csv.reader(csvinput)

    row = next(reader)
    row.append('Rank')
    all.append(row)

    data_to_add = ["01", "02", "03"]
    i = 0

    for row in reader:
        row.append(random.choice(data_to_add))
        all.append(row)
        i = i + 1



with open('test.csv', 'w') as csvoutput:
    writer = csv.writer(csvoutput, lineterminator='\n')
    writer.writerows(all)