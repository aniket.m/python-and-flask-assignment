cityName = ""

class City:

    def __init__(self):
        self.cityName = "Pune"
        self.cityCode = "411039"

    def getCityName(self):
        return self.cityName

    def setCityName(self, name):
        self.cityName = name

    def setCityCode(self, code):
        self.cityCode = code
    
    def getCityCode(self):
        return self.cityCode