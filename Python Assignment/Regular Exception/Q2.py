from itertools import groupby
import re
a=[3,4,9]
pattern = re.compile(r'(?:\d{4}-){3}\d{4}|\d{16}')

num = '6244-5567-8912-3458'

def count_consecutive(num):
    return max(len(list(g)) for _, g in groupby(num))

print (count_consecutive(num.replace('-', '')))

def

if (not pattern.fullmatch(num) or count_consecutive(num.replace('-', '')) >= 4) or (num[0] not in a):
    
    print('Failed')
    
else:
    print('Success')
